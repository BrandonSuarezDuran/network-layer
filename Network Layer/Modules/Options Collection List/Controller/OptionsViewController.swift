//
//  OptionsViewController.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/13/21.
//

import UIKit

class OptionsViewController: UIViewController {
    @IBOutlet private weak var searchBar: UISearchBar! {
        didSet {
            self.searchBar.backgroundImage = nil
        }
    }
    @IBOutlet private weak var optionsListCollectionView: UICollectionView! {
        didSet {
            self.optionsListCollectionView.dataSource = self
            self.optionsListCollectionView.delegate = self
            self.optionsListCollectionView.layer.backgroundColor = nil
        }
    }
    private var data: [String] = ["Your Orders", "Buy Again", "Your List", "Your Account", "Programs & Features", "Shop by Department", "Settings", "Customer Service"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension OptionsViewController: UICollectionViewDelegate {
}

extension OptionsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = optionsListCollectionView.dequeueReusableCell(withReuseIdentifier: "OptionsCollectionViewCell", for: indexPath) as? OptionsCollectionViewCell else {
            fatalError("Cell not dequeued")
        }
        cell.configure(button: data[indexPath.item])
        return cell
    }
}
