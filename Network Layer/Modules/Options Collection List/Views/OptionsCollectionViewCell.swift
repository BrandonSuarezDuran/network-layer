//
//  OptionsCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/13/21.
//

import UIKit

class OptionsCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var optionsButton: UIButton!
    
    func configure(button: String) {
        self.optionsButton.setTitle(button, for: .normal)
    }
}
