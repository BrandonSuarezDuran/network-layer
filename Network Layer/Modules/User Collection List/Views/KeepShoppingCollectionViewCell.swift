//
//  KeepShoppingCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/21/21.
//

import UIKit

class KeepShoppingCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var productImage: UIImageView! {
        didSet {
            self.productImage.layer.masksToBounds = true
            self.productImage.layer.cornerRadius = self.frame.height / 5.0
        }
    }
    @IBOutlet private weak var productNameLabel: UILabel!
    
    func configure(data: [String]) {
        self.productImage.image = UIImage(named: data[0])
        self.productNameLabel.text = data[1]
    }
}
