//
//  GifCardBalanceTableviewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/21/21.
//

import UIKit

class GifCardBalanceTableviewCell: UITableViewCell {
    @IBOutlet private weak var giftCardBalanceLabel: UILabel!
    @IBOutlet private weak var redeemButton: UIButton! {
        didSet {
            self.redeemButton.layer.masksToBounds = true
            self.redeemButton.layer.cornerRadius = self.frame.height / 20.0
            self.redeemButton.layer.borderColor = CGColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
            self.redeemButton.layer.borderWidth = 0.5
        }
    }
    @IBOutlet private weak var reloadButton: UIButton! {
        didSet {
            self.reloadButton.layer.masksToBounds = true
            self.reloadButton.layer.cornerRadius = self.frame.height / 20.0
            self.reloadButton.layer.borderColor = CGColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
            self.reloadButton.layer.borderWidth = 0.5
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
