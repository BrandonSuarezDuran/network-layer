//
//  YourAccountTableViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/21/21.
//

import UIKit

class YourAccountTableViewCell: UITableViewCell {

    @IBOutlet private weak var yourAccountItemsCollectionView: UICollectionView! {
        didSet {
            self.yourAccountItemsCollectionView.delegate = self
            self.yourAccountItemsCollectionView.dataSource = self
        }
    }
    private var buttonName: [String] = ["Your Browsing History", "Gift Cards", "Subscribe & Save", "Prime"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension YourAccountTableViewCell: UICollectionViewDelegate {
}

extension YourAccountTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        buttonName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = yourAccountItemsCollectionView.dequeueReusableCell(withReuseIdentifier: "YourAccountCollectionViewCell", for: indexPath) as? YourAccountCollectionViewCell else {
            fatalError("Cell not dequeued")
        }
        cell.configure(buttonName: buttonName[indexPath.item])
        return cell
    }
}
