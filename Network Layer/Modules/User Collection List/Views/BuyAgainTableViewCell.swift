//
//  BuyAgainTableViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/21/21.
//

import UIKit

class BuyAgainTableViewCell: UITableViewCell {

    @IBOutlet private weak var buyAgainCollectionView: UICollectionView! {
        didSet {
            self.buyAgainCollectionView.delegate = self
            self.buyAgainCollectionView.dataSource = self
        }
    }
    private var data: [String] = ["dod", "sp20", "sp21", "sp22", "sp23", "sp10"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension BuyAgainTableViewCell: UICollectionViewDelegate {
}
extension BuyAgainTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = buyAgainCollectionView.dequeueReusableCell(withReuseIdentifier: "BuyAgainCollectionViewCell", for: indexPath) as? BuyAgainCollectionViewCell else {
            fatalError("Cell not dequeued")
        }
        cell.configure(image: self.data[indexPath.item])
        return cell
    }
}
