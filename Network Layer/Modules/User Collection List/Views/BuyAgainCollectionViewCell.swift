//
//  BuyAgainCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/21/21.
//

import UIKit

class BuyAgainCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var productImage: UIImageView! {
        didSet {
            self.productImage.layer.masksToBounds = true
            self.productImage.layer.cornerRadius = self.frame.height / 5.0
        }
    }
    
    func configure(image: String) {
        self.productImage.image = UIImage(named: image)
    }
}
