//
//  YourAccountCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/21/21.
//

import UIKit

class YourAccountCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var button: UIButton! {
        didSet {
            self.button.layer.cornerRadius = self.frame.height / 4.0
            self.button.layer.borderColor = CGColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
            self.button.layer.borderWidth = 0.5
        }
    }
    
    func configure(buttonName: String) {
        self.button.setTitle(buttonName, for: .normal)
    }
}
