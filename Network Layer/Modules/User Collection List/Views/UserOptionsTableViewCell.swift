//
//  UserOptionsTableViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/13/21.
//

import UIKit

class UserOptionsTableViewCell: UITableViewCell {
    @IBOutlet private weak var yourOrdersButton: UIButton! {
    didSet {
        self.yourOrdersButton.layer.masksToBounds = true
        self.yourOrdersButton.layer.cornerRadius = self.frame.height / 20.0
        self.yourOrdersButton.layer.borderColor = CGColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
        self.yourOrdersButton.layer.borderWidth = 0.5
        }
    }
    @IBOutlet private weak var buyAgainButton: UIButton! {
    didSet {
        self.buyAgainButton.layer.masksToBounds = true
        self.buyAgainButton.layer.cornerRadius = self.frame.height / 20.0
        self.buyAgainButton.layer.borderColor = CGColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
        self.buyAgainButton.layer.borderWidth = 0.5
        }
    }
    @IBOutlet private weak var yourAccountButton: UIButton! {
    didSet {
        self.yourAccountButton.layer.masksToBounds = true
        self.yourAccountButton.layer.cornerRadius = self.frame.height / 20.0
        self.yourAccountButton.layer.borderColor = CGColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
        self.yourAccountButton.layer.borderWidth = 0.5
        }
    }
    @IBOutlet private weak var yourListButton: UIButton! {
    didSet {
        self.yourListButton.layer.masksToBounds = true
        self.yourListButton.layer.cornerRadius = self.frame.height / 20.0
        self.yourListButton.layer.borderColor = CGColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
        self.yourListButton.layer.borderWidth = 0.5
        }
    }
    @IBOutlet private weak var nestedCollectionView: UICollectionView! {
        didSet {
            self.nestedCollectionView.delegate = self
            self.nestedCollectionView.dataSource = self
        }
    }
    private var data: [String] = []
    
    func configure(data: [String]) {
        self.data = data
    }
}

// MARK: - Extensions
extension UserOptionsTableViewCell: UICollectionViewDelegate {
}
extension UserOptionsTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "YourOrdersCollectionViewCell", for: indexPath) as? YourOrdersCollectionViewCell else {
            fatalError("Cell not dequeued")
        }
        cell.configure(image: data[indexPath.item])
        return cell
    }
}
