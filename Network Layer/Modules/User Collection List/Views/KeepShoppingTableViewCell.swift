//
//  KeepShoppingTableViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/21/21.
//

import UIKit

class KeepShoppingTableViewCell: UITableViewCell {
    @IBOutlet private weak var keepShoppingCollectionView: UICollectionView! {
        didSet {
            self.keepShoppingCollectionView.delegate = self
            self.keepShoppingCollectionView.dataSource = self
        }
    }

    // The data format should be image, name of the product
    private var data: [[String]] = [["sp5", "Spring Dress"], ["sp4", "Sony A7"], ["sp7", "Phone Stand"], ["sp8", "Blue Tie"]]
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension KeepShoppingTableViewCell: UICollectionViewDelegate {
}

extension KeepShoppingTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = keepShoppingCollectionView.dequeueReusableCell(withReuseIdentifier: "KeepShoppingCollectionViewCell", for: indexPath) as? KeepShoppingCollectionViewCell else {
            fatalError("Cell not dequeued")
        }
        cell.configure(data: self.data[indexPath.item])
        return cell
    }
}
