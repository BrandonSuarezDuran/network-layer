//
//  YourOrdersCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/13/21.
//

import UIKit
// This is the nested collection view cell
class YourOrdersCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var purchasedProductsImage: UIImageView! {
        didSet {
            self.purchasedProductsImage.layer.masksToBounds = true
            self.purchasedProductsImage.layer.cornerRadius = self.frame.height / 5.0
        }
    }
    
    func configure(image: String) {
        self.purchasedProductsImage.image = UIImage(named: image)
    }
}
