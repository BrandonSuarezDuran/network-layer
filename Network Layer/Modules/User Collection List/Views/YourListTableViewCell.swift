//
//  YourListTableViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/20/21.
//

import UIKit

class YourListTableViewCell: UITableViewCell {

    @IBOutlet private weak var firstListImage: UIImageView! {
        didSet {
            self.firstListImage.layer.masksToBounds = true
            self.firstListImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var firstListNameLabel: UILabel!
    @IBOutlet private weak var firstListCategoryLabel: UILabel!
    
    @IBOutlet private weak var secondListImage: UIImageView! {
        didSet {
            self.secondListImage.layer.masksToBounds = true
            self.secondListImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var secondListNameLabel: UILabel!
    @IBOutlet private weak var secondListCategoryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(firstImage: String, firstName: String, firstCategory: String, secondImage: String, secondName: String, secondCategory: String) {
        
        self.firstListImage.image = UIImage(named: firstImage)
        self.firstListNameLabel.text = firstName
        self.firstListCategoryLabel.text = firstCategory
        self.secondListImage.image = UIImage(named: secondImage)
        self.secondListNameLabel.text = secondName
        self.secondListCategoryLabel.text = secondCategory
    }
}
