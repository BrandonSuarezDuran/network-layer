//
//  UserViewController.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/13/21.
//

import UIKit

class UserViewController: UIViewController {

    @IBOutlet private weak var userImage: UIImageView! {
        didSet {
            self.userImage.layer.masksToBounds = true
            self.userImage.layer.cornerRadius = self.userImage.frame.height / 4.0
        }
    }
    @IBOutlet private weak var userOptionsTableView: UITableView! {
        didSet {
            self.userOptionsTableView.dataSource = self
            self.userOptionsTableView.delegate = self
            self.userOptionsTableView.register(UINib(nibName: "YourListCell", bundle: Bundle.main), forCellReuseIdentifier: "YourListTableViewCell")
            self.userOptionsTableView.tableFooterView = UIView()
        }
    }
    // MARK: - Data for the first TableViewCell , the nested Collection
    private var data: [String] = ["dod", "sp19", "sp13", "sp11", "sp21"]
    // MARK: - Data for the second TableViewCell, The register NIB YourListTableViewCell
    private var yourListData: [String] = ["sp9", "My Wish List", "Private - Default", "sp10", "My List", "Public"]
    // ------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension UserViewController: UITableViewDelegate {
}

extension UserViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        switch indexPath.row {
        case 0:
            guard let userCell = userOptionsTableView.dequeueReusableCell(withIdentifier: "UserOptionsTableViewCell", for: indexPath) as? UserOptionsTableViewCell else {
                fatalError("Cell not dequeued")
            }
            userCell.selectionStyle = .none
            userCell.configure(data: self.data)
            cell = userCell
            
        case 1:
            guard let yourListCell = userOptionsTableView.dequeueReusableCell(withIdentifier: "YourListTableViewCell", for: indexPath) as? YourListTableViewCell else {
                fatalError("Cell not dequeued")
            }
            yourListCell.selectionStyle = .none
            yourListCell.configure(firstImage: yourListData[0],
                                   firstName: yourListData[1],
                                   firstCategory: yourListData[2],
                                   secondImage: yourListData[3],
                                   secondName: yourListData[4],
                                   secondCategory: yourListData[5])
            cell = yourListCell
        
        case 2:
            guard let yourAccountCell = userOptionsTableView.dequeueReusableCell(withIdentifier: "YourAccountTableViewCell", for: indexPath) as? YourAccountTableViewCell else {
                fatalError("Cell not dequeued")
            }
            yourAccountCell.selectionStyle = .none
            cell = yourAccountCell
            
        case 3:
            guard let keepShoppingCell = userOptionsTableView.dequeueReusableCell(withIdentifier: "KeepShoppingTableViewCell", for: indexPath) as? KeepShoppingTableViewCell else {
                fatalError("Cell not dequeued")
            }
            keepShoppingCell.selectionStyle = .none
            cell = keepShoppingCell
        
        case 4:
            guard let gifCardBalanceCell = userOptionsTableView.dequeueReusableCell(withIdentifier: "GifCardBalanceTableviewCell", for: indexPath) as? GifCardBalanceTableviewCell else {
                fatalError("Cell not dequeued")
            }
            gifCardBalanceCell.selectionStyle = .none
            cell = gifCardBalanceCell
            
        case 5:
            guard let buyAgainCell = userOptionsTableView.dequeueReusableCell(withIdentifier: "BuyAgainTableViewCell", for: indexPath) as? BuyAgainTableViewCell else {
                fatalError("Cell not dequeued")
            }
            buyAgainCell.selectionStyle = .none
            cell = buyAgainCell
            
        default:
            break
        }
        return cell
    }
}
