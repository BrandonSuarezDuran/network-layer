//
//  DealOfTheDayCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/23/21.
//

import UIKit

class DealOfTheDayCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var productImage: UIImageView! {
        didSet {
            self.productImage.layer.masksToBounds = true
            self.productImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var backView: UIView! {
        didSet {
            self.backView.layer.masksToBounds = true
            self.backView.layer.cornerRadius = self.frame.height / 40.0
        }
    }
    @IBOutlet private weak var promotionLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var dealEndsTimeLabel: UILabel!
    
    func configure(image: String, promotion: String, price: String, endTime: String) {
        self.productImage.image = UIImage(named: image)
        self.promotionLabel.text = promotion
        self.priceLabel.text = price
        self.dealEndsTimeLabel.text = endTime
    }
}
