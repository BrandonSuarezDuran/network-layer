//
//  OneImageProductCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/25/21.
//

import UIKit

class OneImageProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var productImage: UIImageView! {
        didSet {
            self.productImage.layer.masksToBounds = true
            self.productImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    
    func configure(image: String) {
        self.productImage.image = UIImage(named: image)
    }
}
