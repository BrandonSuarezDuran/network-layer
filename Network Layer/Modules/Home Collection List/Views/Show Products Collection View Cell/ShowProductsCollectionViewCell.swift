//
//  ShowProductsCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/12/21.
//

import UIKit

class ShowProductsCollectionViewCell: UICollectionViewCell {
    // First Product image an label
    @IBOutlet private weak var firstProductImage: UIImageView! {
        didSet {
            self.firstProductImage.layer.masksToBounds = true
            self.firstProductImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var firstProductNameLabel: UILabel!
    @IBOutlet private weak var firstProductPriceLabel: UILabel!
    @IBOutlet private weak var firstViewContainer: UIView! {
        didSet {
            self.firstViewContainer.layer.masksToBounds = true
            self.firstViewContainer.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    // Second Product image and label
    @IBOutlet private weak var secondProductImage: UIImageView! {
        didSet {
            self.secondProductImage.layer.masksToBounds = true
            self.secondProductImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var secondProductNameLabel: UILabel!
    @IBOutlet private weak var secondProductPriceLabel: UILabel!
    @IBOutlet private weak var secondViewContainer: UIView! {
        didSet {
            self.secondViewContainer.layer.masksToBounds = true
            self.secondViewContainer.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    // Third Product image and label
    @IBOutlet private weak var thirdProductImage: UIImageView! {
        didSet {
            self.thirdProductImage.layer.masksToBounds = true
            self.thirdProductImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var thirdProductNameLabel: UILabel!
    @IBOutlet private weak var thirdProductPriceLabel: UILabel!
    @IBOutlet private weak var thirdViewContainer: UIView! {
        didSet {
            self.thirdViewContainer.layer.masksToBounds = true
            self.thirdViewContainer.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    // Fourth Product image and label
    @IBOutlet private weak var fourthProductImage: UIImageView! {
        didSet {
            self.fourthProductImage.layer.masksToBounds = true
            self.fourthProductImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var fourthProductNameLabel: UILabel!
    @IBOutlet private weak var fourthProductPriceLabel: UILabel!
    @IBOutlet private weak var fourthViewContainer: UIView! {
        didSet {
            self.fourthViewContainer.layer.masksToBounds = true
            self.fourthViewContainer.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    
    @IBOutlet private weak var slidingOffersCollectionView: UICollectionView! {
        didSet {
            self.slidingOffersCollectionView.dataSource = self
            self.slidingOffersCollectionView.delegate = self
            self.slidingOffersCollectionView.backgroundColor = nil
        }
    }
    private var stagedImages: [String] = ["sp1", "sp2", "sp3", "sp4", "sp5", "sp6"]
    
    func configure(product: [String]) {
        
        // First Product
        self.firstProductImage.image = UIImage(named: product[0])
        self.firstProductNameLabel.text = product[1]
        self.firstProductPriceLabel.text = product[2]
        // Second Product
        self.secondProductImage.image = UIImage(named: product[3])
        self.secondProductNameLabel.text = product[4]
        self.secondProductPriceLabel.text = product[5]
        // Third Product
        self.thirdProductImage.image = UIImage(named: product[6])
        self.thirdProductNameLabel.text = product[7]
        self.thirdProductPriceLabel.text = product[8]
        // Fourth Product
        self.fourthProductImage.image = UIImage(named: product[9])
        self.fourthProductNameLabel.text = product[10]
        self.fourthProductPriceLabel.text = product[11]
    }
}

extension ShowProductsCollectionViewCell: UICollectionViewDelegate {
}

extension ShowProductsCollectionViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        stagedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let stagedProductsCell = slidingOffersCollectionView.dequeueReusableCell(withReuseIdentifier: "SlidingOffersCollectionViewCell", for: indexPath) as?
                SlidingOffersCollectionViewCell else {
            fatalError("Cell not dequeued")
        }
        stagedProductsCell.configure(image: stagedImages[indexPath.item])
        return stagedProductsCell
    }
}
