//
//  SlidingOffersCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/13/21.
//

import UIKit

// This is the nested collection view cell
class SlidingOffersCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var productImage: UIImageView! {
        didSet {
            // setting the decoration
            // This is to apply a corner radius
            self.productImage.layer.masksToBounds = true
            self.productImage.layer.cornerRadius = self.frame.height / 5.0
        }
    }
    
    func configure(image: String) {
        self.productImage.image = UIImage(named: image)
    }
}
