//
//  EightCellsCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/25/21.
//

import UIKit

class EightCellsCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var firstImage: UIImageView! {
        didSet {
            self.firstImage.layer.masksToBounds = true
            self.firstImage.layer.cornerRadius = self.frame.height / 40.0
        }
    }
    @IBOutlet private weak var secondImage: UIImageView! {
        didSet {
            self.secondImage.layer.masksToBounds = true
            self.secondImage.layer.cornerRadius = self.frame.height / 40.0
        }
    }
    @IBOutlet private weak var thirdImage: UIImageView! {
        didSet {
            self.thirdImage.layer.masksToBounds = true
            self.thirdImage.layer.cornerRadius = self.frame.height / 40.0
        }
    }
    @IBOutlet private weak var fourthImage: UIImageView! {
        didSet {
            self.fourthImage.layer.masksToBounds = true
            self.fourthImage.layer.cornerRadius = self.frame.height / 40.0
        }
    }
    @IBOutlet private weak var fifthImage: UIImageView! {
        didSet {
            self.fifthImage.layer.masksToBounds = true
            self.fifthImage.layer.cornerRadius = self.frame.height / 40.0
        }
    }
    @IBOutlet private weak var sixthImage: UIImageView! {
        didSet {
            self.sixthImage.layer.masksToBounds = true
            self.sixthImage.layer.cornerRadius = self.frame.height / 40.0
        }
    }
    @IBOutlet private weak var seventhImage: UIImageView! {
        didSet {
            self.seventhImage.layer.masksToBounds = true
            self.seventhImage.layer.cornerRadius = self.frame.height / 40.0
        }
    }
    @IBOutlet private weak var eighthImage: UIImageView! {
        didSet {
            self.eighthImage.layer.masksToBounds = true
            self.eighthImage.layer.cornerRadius = self.frame.height / 40.0
        }
    }
    @IBOutlet private weak var firstCategoryLabel: UILabel!
    @IBOutlet private weak var secondCategoryLabel: UILabel!
    @IBOutlet private weak var thirdCategoryLabel: UILabel!
    @IBOutlet private weak var fourthCategoryLabel: UILabel!
    @IBOutlet private weak var fifthCategoryLabel: UILabel!
    @IBOutlet private weak var sixthCategoryLabel: UILabel!
    @IBOutlet private weak var seventhCategoryLabel: UILabel!
    @IBOutlet private weak var eighthCategoryLabel: UILabel!
    
    @IBOutlet private weak var firstCategoryContainerView: UIView! {
        didSet {
            self.firstCategoryContainerView.layer.masksToBounds = true
            self.firstCategoryContainerView.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    @IBOutlet private weak var secondCategoryContainerView: UIView! {
        didSet {
            self.secondCategoryContainerView.layer.masksToBounds = true
            self.secondCategoryContainerView.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    @IBOutlet private weak var thirdCategoryContainerView: UIView! {
        didSet {
            self.thirdCategoryContainerView.layer.masksToBounds = true
            self.thirdCategoryContainerView.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    @IBOutlet private weak var fourthCategoryContainerView: UIView! {
        didSet {
            self.fourthCategoryContainerView.layer.masksToBounds = true
            self.fourthCategoryContainerView.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    @IBOutlet private weak var fifthCategoryContainerView: UIView! {
        didSet {
            self.fifthCategoryContainerView.layer.masksToBounds = true
            self.fifthCategoryContainerView.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    @IBOutlet private weak var sixthCategoryContainerView: UIView! {
        didSet {
            self.sixthCategoryContainerView.layer.masksToBounds = true
            self.sixthCategoryContainerView.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    @IBOutlet private weak var seventhCategoryContainerView: UIView! {
        didSet {
            self.seventhCategoryContainerView.layer.masksToBounds = true
            self.seventhCategoryContainerView.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    @IBOutlet private weak var eighthCategoryContainerView: UIView! {
        didSet {
            self.eighthCategoryContainerView.layer.masksToBounds = true
            self.eighthCategoryContainerView.layer.cornerRadius = self.frame.height / 60.0
        }
    }

    func configure(data: [String]) {
        self.firstImage.image = UIImage(named: data[0])
        self.secondImage.image = UIImage(named: data[2])
        self.thirdImage.image = UIImage(named: data[4])
        self.fourthImage.image = UIImage(named: data[6])
        self.fifthImage.image = UIImage(named: data[8])
        self.sixthImage.image = UIImage(named: data[10])
        self.seventhImage.image = UIImage(named: data[12])
        self.eighthImage.image = UIImage(named: data[14])
        self.firstCategoryLabel.text = data[1]
        self.secondCategoryLabel.text = data[3]
        self.thirdCategoryLabel.text = data[5]
        self.fourthCategoryLabel.text = data[7]
        self.fifthCategoryLabel.text = data[9]
        self.sixthCategoryLabel.text = data[11]
        self.seventhCategoryLabel.text = data[13]
        self.eighthCategoryLabel.text = data[15]
    }
    func roundCornersDecoration() {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.frame.height / 20.0
    }
}
