//
//  CategoriesCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/11/21.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var categoriesButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(buttonTittle: String) {
        self.categoriesButton.setTitle(buttonTittle, for: .normal)
    }
}
