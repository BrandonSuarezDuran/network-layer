//
//  RelatedPicksCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/24/21.
//

import UIKit

class RelatedPicksCollectionViewCell: UICollectionViewCell {
    // MARK: - First Element
    @IBOutlet private weak var firstImage: UIImageView! {
        didSet {
            self.firstImage.layer.masksToBounds = true
            self.firstImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var firstViewContainer: UIView! {
        didSet {
            self.firstViewContainer.layer.masksToBounds = true
            self.firstViewContainer.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    @IBOutlet private weak var firstDescriptionLabel: UILabel!
    @IBOutlet private weak var firstPriceTagLabel: UILabel!
    // MARK: - Second Element
    @IBOutlet private weak var secondImage: UIImageView! {
        didSet {
            self.secondImage.layer.masksToBounds = true
            self.secondImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var secondViewContainer: UIView! {
        didSet {
            self.secondViewContainer.layer.masksToBounds = true
            self.secondViewContainer.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    @IBOutlet private weak var secondDescriptionLabel: UILabel!
    @IBOutlet private weak var secondPriceTagLabel: UILabel!
    // MARK: - Third Element
    @IBOutlet private weak var thirdImage: UIImageView! {
        didSet {
            self.thirdImage.layer.masksToBounds = true
            self.thirdImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var thirdViewContainer: UIView! {
        didSet {
            self.thirdViewContainer.layer.masksToBounds = true
            self.thirdViewContainer.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    @IBOutlet private weak var thirdDescriptionLabel: UILabel!
    @IBOutlet private weak var thirdPriceTagLabel: UILabel!
    // MARK: - Fourth Element
    @IBOutlet private weak var fourthImage: UIImageView! {
        didSet {
            self.fourthImage.layer.masksToBounds = true
            self.fourthImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var fourthViewContainer: UIView! {
        didSet {
            self.fourthViewContainer.layer.masksToBounds = true
            self.fourthViewContainer.layer.cornerRadius = self.frame.height / 60.0
        }
    }
    @IBOutlet private weak var fourthDescriptionLabel: UILabel!
    @IBOutlet private weak var fourthPriceTagLabel: UILabel!
    
    func configure(data: [String]) {
        firstImage.image = UIImage(named: data[0])
        firstDescriptionLabel.text = data[1]
        firstPriceTagLabel.text = data[2]
        secondImage.image = UIImage(named: data[3])
        secondDescriptionLabel.text = data[4]
        secondPriceTagLabel.text = data[5]
        thirdImage.image = UIImage(named: data[6])
        thirdDescriptionLabel.text = data[7]
        thirdPriceTagLabel.text = data[8]
        fourthImage.image = UIImage(named: data[9])
        fourthDescriptionLabel.text = data[10]
        fourthPriceTagLabel.text = data[11]
    }
}
