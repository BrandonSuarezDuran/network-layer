//  ProductCollection.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/7/21.
//

import Foundation
import UIKit

struct ProductCollection: Decodable {
    var customCollection: [ProductInfo]
}

struct ProductInfo: Decodable {
    var id: Int
    var title: String
    var price: Double
    var description: String
    var category: String
    var image: String
}
