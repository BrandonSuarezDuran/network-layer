//  ProductCollectionViewController.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/8/21.
//

import UIKit
class ProductCollectionViewController: UIViewController {
    
    @IBOutlet private weak var searchBar: UISearchBar! {
        didSet {
            self.searchBar.backgroundImage = nil
        }
    }
    @IBOutlet private weak var addressView: UIView! {
        didSet {
            self.addressView.layer.masksToBounds = true
            self.addressView.layer.cornerRadius = self.addressView.frame.height / 5.0
        }
    }
    // First collection view - Categories
    @IBOutlet private weak var categoriesCollectionView: UICollectionView! {
        didSet {
            self.categoriesCollectionView.delegate = self
            self.categoriesCollectionView.dataSource = self
            self.categoriesCollectionView.backgroundColor = nil
        }
    }
    // Second Collection view - Offers
    @IBOutlet private weak var offersCollectionView: UICollectionView! {
        didSet {
            self.offersCollectionView.delegate = self
            self.offersCollectionView.dataSource = self
            self.offersCollectionView.backgroundColor = nil
        }
    }
    // This is the inner collection view. (Nested type) - SlidingOffers
    @IBOutlet private weak var slidingOffersCollectionView: UICollectionView! {
        didSet {
            self.slidingOffersCollectionView.delegate = self
            self.slidingOffersCollectionView.dataSource = self
        }
    }
    // -------------------------------------------------
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }
    // Hard Coded Data
    // -> For the categories Collection View
    private var productsCategories: [String] = ["Men's Clothing", "Women's Clothing", "Electronics", "Jewelry", "Sports Gear", "Books", "Video Games", "Home Decor"]
    // -> For the offersCollectionViewCell
    private var stagedProducts: [String] = ["sp11", "sp13", "sp12", "sp15"]
    // -> For the ShowProductsCollectionViewCell
    private var showProducts: [[String]] = [
        ["sp1", "NotePad", "5.00$", "sp5", "Flowered Dress", "20.00$", "sp7", "Boots and Tie bundle", "59.00$", "sp8", "Sony alpha 7", "3499.99$"],
        ["sp1", "NotePad", "5.00$", "sp2", "Google Pod", "20.00$", "sp3", "Moisturized Cream", "5.00$", "sp4", "Sony alpha 7", "3499.99$"]
    ]
    private var dealOfDayData: [String] = ["dod", "20% off, for new costumers", "$35.99", "Deal ends on 04-29"]
    // -> Third Cell's Data, Related Items
    private var relatedItemsData: [String] = ["sp11", "Red AirPods Case", "$19.99", "sp12", "Fancy hand made Bag Pack", "$59.99", "sp13", "Cool T-Shirt", "$40.99", "sp14", "M1 Chip iMac", "$2299.99"]
    // -> Fourth Cell's data, Related Picks Cell
    private var relatedPicksData: [String] = ["sp15", "Xbox One Controller", "$59.99", "sp19", "Canon 6D", "$1299.99", "sp17", "Comfy bed", "$159.99", "sp18", "Nintendo Switch", "$299.99"]
    // -> Fifth Cell's Data, Discount categories
    private var discountData: [String] = [
        "sp24", "Gym Gear",
        "sp25", "Art Accessories",
        "sp26", "Clothes",
        "sp27", "Lenses",
        "sp28", "Cups",
        "sp29", "Electronics",
        "sp30", "Bar Bells",
        "sp31", "Make up",
        "sp32", "Boxing Gear"
    ]
    // -> Sixth Cell's Data, Staged Product
    private var imageData: String = "sp33"
    var dataSource: ProductCollection? {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("-----------------")
    }
}

extension ProductCollectionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoriesCollectionView {
            return productsCategories.count
        }
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        // MARK: - Categories Cell - Sliding Top Menu
        guard let categoryCell = categoriesCollectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCollectionViewCell", for: indexPath)
                as? CategoriesCollectionViewCell else {
            fatalError("Cell not dequeued") }
        categoryCell.configure(buttonTittle: productsCategories[indexPath.item])
        
        switch indexPath.row {
        // MARK: - First Cell
        case 0:
            guard let showOffProductCell = offersCollectionView.dequeueReusableCell(withReuseIdentifier: "ShowProductsCollectionViewCell", for: indexPath) as? ShowProductsCollectionViewCell else {
                fatalError("Cell not dequeued")
            }
            cell = showOffProductCell
        // MARK: - Second cell
        case 1:
            guard let dealOfDayCell = offersCollectionView.dequeueReusableCell(withReuseIdentifier: "DealOfTheDayCollectionViewCell", for: indexPath) as? DealOfTheDayCollectionViewCell else {
                fatalError("Cell not dequeued")
            }
            dealOfDayCell.configure(image: dealOfDayData[0], promotion: dealOfDayData[1], price: dealOfDayData[2], endTime: dealOfDayData[3])
            cell = dealOfDayCell
        // MARK: - Third Cell
        case 2:
            guard let relatedItemCell = offersCollectionView.dequeueReusableCell(withReuseIdentifier: "RelatedItemsCollectionViewCell", for: indexPath) as? RelatedItemsCollectionViewCell else {
                fatalError("Fourth Cell not dequeued")
            }
            relatedItemCell.configure(data: relatedItemsData)
            cell = relatedItemCell
        // MARK: - Fourth Cell
        case 3:
            guard let relatedPicksItemCell = offersCollectionView.dequeueReusableCell(withReuseIdentifier: "RelatedPicksCollectionViewCell", for: indexPath) as? RelatedPicksCollectionViewCell else {
                fatalError("Fourth Cell not dequeued")
            }
            relatedPicksItemCell.configure(data: relatedPicksData )
            cell = relatedPicksItemCell
        // MARK: - Fifth Cell
        case 4:
            guard let eighthImagesCell = offersCollectionView.dequeueReusableCell(withReuseIdentifier: "EightCellsCollectionViewCell", for: indexPath) as? EightCellsCollectionViewCell else {
                fatalError("Fifth Cell  not dequeued")
            }
            eighthImagesCell.configure(data: discountData)
            cell = eighthImagesCell
        // MARK: - = Sixth Cell
        case 5:
            guard let oneImageCell = offersCollectionView.dequeueReusableCell(withReuseIdentifier: "OneImageProductCollectionViewCell", for: indexPath) as? OneImageProductCollectionViewCell else {
                fatalError("Sixth Cell not dequeued")
            }
            oneImageCell.configure(image: imageData)
            cell = oneImageCell
            
        default:
            break
        }
        
        if collectionView == categoriesCollectionView {
            return categoryCell
        }
        return cell
    }
}

extension ProductCollectionViewController: UICollectionViewDelegate {
}
