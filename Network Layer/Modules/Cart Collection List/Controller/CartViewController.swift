//
//  CartViewController.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/13/21.
//

import UIKit

class CartViewController: UIViewController {

    @IBOutlet private weak var searchBar: UISearchBar! {
        didSet {
            self.searchBar.backgroundImage = nil
        }
    }
    @IBOutlet private weak var cartCategoriesCollectionView: UICollectionView! {
        didSet {
            self.cartCategoriesCollectionView.delegate = self
            self.cartCategoriesCollectionView.dataSource = self
            self.cartCategoriesCollectionView.backgroundColor = nil
        }
    }
    @IBOutlet private weak var addressView: UIView! {
        didSet {
            self.addressView.layer.masksToBounds = true
            self.addressView.layer.cornerRadius = self.addressView.frame.height / 5.0
        }
    }
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.tableFooterView = UIView()
        }
    }
    private var categoriesName: [String] = ["Cart", "Buy Again", "In-Store code", "Books & 4-start", "Amazon Go", "Fresh In-Store"]
    private var subtotalData: [String] = ["1", "10.99"]
    private var currentProductData: [String] = ["sp8", "Wool hand made Tie", "Men Clothes", "$15.99"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension CartViewController: UICollectionViewDelegate {
}

extension CartViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cartCategoriesCollectionView {
            return categoriesName.count
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cartCategoriesCell = cartCategoriesCollectionView.dequeueReusableCell(withReuseIdentifier: "cartCategoriesCollectionViewCell",
                                                                                        for: indexPath) as? CartCategoriesCollectionViewCell else {
            fatalError("Cell not dequeued")
        }
        cartCategoriesCell.configure(name: categoriesName[indexPath.item])
        return cartCategoriesCell
    }
}

extension CartViewController: UITableViewDelegate {
}

extension CartViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        switch indexPath.row {
        case 0:
            guard let checkOutCell = tableView.dequeueReusableCell(withIdentifier: "SubtotalTableViewCell", for: indexPath) as? SubtotalTableViewCell else {
                fatalError("Cell not dequeued")
            }
            checkOutCell.configure(data: subtotalData)
            cell = checkOutCell
        
        case 1:
            guard let currentProduct = tableView.dequeueReusableCell(withIdentifier: "CurrentProductTableViewCell", for: indexPath) as? CurrentProductTableViewCell else {
                fatalError("Cell not dequeued")
            }
            currentProduct.configure(data: currentProductData)
            cell = currentProduct
        
        case 2:
            guard let sponsoredProductCell = tableView.dequeueReusableCell(withIdentifier: "SponsoredProductTableViewCell", for: indexPath) as? SponsoredProductTableViewCell else {
                fatalError("Cell not dequeued")
            }
            cell = sponsoredProductCell
            
        case 3:
            guard let recommendedProductCell = tableView.dequeueReusableCell(withIdentifier: "RecommendedProductTableViewCell", for: indexPath) as? RecommendedProductTableViewCell else {
                fatalError("Cell not dequeued")
            }
            cell = recommendedProductCell
        
        case 4:
            guard let topPicksCell = tableView.dequeueReusableCell(withIdentifier: "TopPicksTableViewCell", for: indexPath) as? TopPicksTableViewCell else {
                fatalError("Cell not dequeued")
            }
            cell = topPicksCell
            
        case 5:
            guard let recommendationCell = tableView.dequeueReusableCell(withIdentifier: "RecommendationsTableViewCell", for: indexPath) as? RecommendationsTableViewCell else {
                fatalError("Cell not dequeued")
            }
            cell = recommendationCell
            
        default:
            break
        }
        cell.selectionStyle = .none
        return cell
    }
}
