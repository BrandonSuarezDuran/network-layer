//
//  SponsoredProductTableViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/22/21.
//

import UIKit

class SponsoredProductTableViewCell: UITableViewCell {

    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            self.collectionView.dataSource = self
            self.collectionView.delegate = self
        }
    }
    
    private var sponsoredProductsData: [[String]] = [
        ["sp1", "All purpose hand made lavender notepad", "$9.99"],
        ["sp2", "All purpose smart speaker", "$19.99"],
        ["sp4", "Brand new Sony A7", "$2000.00"],
        ["sp5", "Spring Dress", "$39.59"]
    ]
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension SponsoredProductTableViewCell: UICollectionViewDelegate {
}

extension SponsoredProductTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        sponsoredProductsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StagedProductCollectionViewCell", for: indexPath) as? StagedProductCollectionViewCell else {
            fatalError("Cell not dequeued")
        }
        cell.configure(data: sponsoredProductsData[indexPath.item])
        return cell
    }
}
