//
//  StagedProductCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/22/21.
//

import UIKit

class StagedProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var productImage: UIImageView! {
        didSet {
            self.productImage.layer.masksToBounds = true
            self.productImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var descriptionButton: UIButton!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var buyingOptionsButton: UIButton! {
        didSet {
            self.buyingOptionsButton.layer.masksToBounds = true
            self.buyingOptionsButton.layer.cornerRadius = self.frame.height / 20.0
            self.buyingOptionsButton.layer.borderColor = CGColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
            self.buyingOptionsButton.layer.borderWidth = 0.5
        }
    }
    
    func configure(data: [String]) {
        self.productImage.image = UIImage(named: data[0])
        self.descriptionButton.setTitle(data[1], for: .normal)
        self.priceLabel.text = data[2]
    }
}
