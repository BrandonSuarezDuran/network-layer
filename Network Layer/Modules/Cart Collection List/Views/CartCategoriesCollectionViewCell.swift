//
//  CartCategoriesCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/21/21.
//

import UIKit

class CartCategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var categoriesButtons: UIButton!
    
    func configure(name: String) {
        self.categoriesButtons.setTitle(name, for: .normal)
    }
}
