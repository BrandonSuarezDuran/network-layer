//
//  RecommendationsTableViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/23/21.
//

import UIKit

class RecommendationsTableViewCell: UITableViewCell {

    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            self.collectionView.dataSource = self
        }
    }
    
    private var data: [[String]] = [
        ["sp15", "Xbox one Controller", "$59.50"],
        ["sp17", "Comfy bed", "$199.99"],
        ["sp18", "Nintendo Switch ExaData Version", "$4500.99"],
        ["sp19", "Canon 6D", "$2299.99"]
    ]
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
extension RecommendationsTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecommendationsCollectionViewCell", for: indexPath) as? RecommendationsCollectionViewCell else {
            fatalError("Cell not dequeued")
        }
        cell.configure(data: data[indexPath.row])
        return cell
    }
}
