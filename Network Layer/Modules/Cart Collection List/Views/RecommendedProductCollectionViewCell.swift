//
//  RecommendedProductCollectionViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/22/21.
//

import UIKit

class RecommendedProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var productImage: UIImageView! {
        didSet {
            self.productImage.layer.masksToBounds = true
            self.productImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var descriptionButton: UIButton!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var addToCartButton: UIButton! {
        didSet {
            self.addToCartButton.layer.masksToBounds = true
            self.addToCartButton.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    
    func configure(data: [String]) {
        self.productImage.image = UIImage(named: data[0])
        self.descriptionButton.setTitle(data[1], for: .normal)
        self.priceLabel.text = data[2]
    }
}
