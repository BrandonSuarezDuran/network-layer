//
//  CurrentProductTableViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/21/21.
//

import UIKit

class CurrentProductTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var productImage: UIImageView! {
        didSet {
            self.productImage.layer.masksToBounds = true
            self.productImage.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    @IBOutlet private weak var productDescriptionLabel: UILabel!
    @IBOutlet private weak var productCategoryLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var availabilityButton: UIButton!
    @IBOutlet private weak var deleteButton: UIButton! {
        didSet {
            self.deleteButton.layer.masksToBounds = true
            self.deleteButton.layer.cornerRadius = self.frame.height / 20.0
            self.deleteButton.layer.borderColor = CGColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
            self.deleteButton.layer.borderWidth = 0.5
        }
    }
    @IBOutlet private weak var saveForLaterButton: UIButton! {
        didSet {
            self.saveForLaterButton.layer.masksToBounds = true
            self.saveForLaterButton.layer.cornerRadius = self.frame.height / 20.0
            self.saveForLaterButton.layer.borderColor = CGColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
            self.saveForLaterButton.layer.borderWidth = 0.5
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func configure(data: [String]) {
        self.productImage.image = UIImage(named: data[0])
        self.productDescriptionLabel.text = data[1]
        self.productCategoryLabel.text = data[2]
        self.priceLabel.text = data[3]
    }
}
