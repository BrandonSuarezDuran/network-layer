//
//  TopPicksTableViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/23/21.
//

import UIKit

class TopPicksTableViewCell: UITableViewCell {

    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            self.collectionView.dataSource = self
        }
    }
    
    private var data: [[String]] = [
        ["sp12", "Handy bag Pack", "$30.00"],
        ["sp13", "Cool T-Shirt", "$19.99"],
        ["sp14", "iMac 2021", "$2999.99"],
        ["sp15", "Xbox one Controller", "$59.50"]
    ]
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension TopPicksTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopPicksCollectionViewCell", for: indexPath) as? TopPicksCollectionViewCell else {
            fatalError("Cell not dequeued")
        }
        cell.configure(data: data[indexPath.item])
        return cell
    }
}
