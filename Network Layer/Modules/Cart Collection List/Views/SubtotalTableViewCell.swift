//
//  SubtotalTableViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/21/21.
//

import UIKit

class SubtotalTableViewCell: UITableViewCell {
    @IBOutlet private weak var numberOfElementsLabel: UILabel!
    @IBOutlet private weak var elementPriceLabel: UILabel!
    @IBOutlet private weak var checkoutButton: UIButton! {
        didSet {
            self.checkoutButton.layer.masksToBounds = true
            self.checkoutButton.layer.cornerRadius = self.frame.height / 20.0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(data: [String]) {
        guard let element = Int(data[0]) else {
            return
        }
        if element > 1 {
            numberOfElementsLabel.text = "Subtotal (\(element)) items:"
        } else {
            numberOfElementsLabel.text = "Subtotal (\(element)) item:"
        }
        elementPriceLabel.text = "$\(data[1])"
    }
}
