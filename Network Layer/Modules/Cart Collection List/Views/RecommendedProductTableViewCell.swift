//
//  RecommendedProductTableViewCell.swift
//  Network Layer
//
//  Created by Brandon Suarez on 4/22/21.
//

import UIKit

class RecommendedProductTableViewCell: UITableViewCell {

    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }
    private var data: [[String]] = [
        ["sp2", "Description 2", "$10.00"],
        ["sp3", "Description 3", "$9.99"],
        ["sp4", "Description 4", "$19.99"],
        ["sp5", "Description 5", "$20.50"]
    ]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension RecommendedProductTableViewCell: UICollectionViewDelegate {
}

extension RecommendedProductTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecommendedProductCollectionViewCell", for: indexPath) as? RecommendedProductCollectionViewCell else {
            fatalError("Cell not dequeued")
        }
        cell.configure(data: data[indexPath.item])
        return cell
    }
}
