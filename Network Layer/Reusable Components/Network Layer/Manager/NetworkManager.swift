//
//  NetworkManager.swift
//  Network Layer
//
//  Created by Brandon Suarez on 5/3/21.
//

import Foundation
import UIKit

struct NetworkManager {
    static let environment: NetworkEnvironment = .production // Remove
    static let MovieAPIKey = "https://fakestoreapi.com/products"
    private let router = Router<MovieApi>()
}

enum NetworkResponse: String {
    case success
    case authenticationError = "You need to be authenticated."
    case badRequest = "Bad Request."
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data decoded."
    case unableToDecode = "We could not decode the response."
}

enum Result<String> {
    case success
    case failure(String)
}

fileprivate func handleNetworkResponse(_ response: HTTPResponse) -> Result<String> {
    switch response.statusCode {
    case 200...299: return .success
    case 401...500: return .failure(NetworkResponse.authenticationError.rawValue)
    case 501...599: return .failure(NetworkResponse.outdated.rawValue)
    case 600: return .failure(NetworkResponse.outdated.rawValue)
    default: return .failure(NetworkResponse.failed.rawValue)
    }
}

