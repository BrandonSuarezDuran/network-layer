//
//  URLParameterEncoder.swift
//  Network Layer
//
//  Created by Brandon Suarez on 5/3/21.
//

import UIKit

public struct URLParameterEncoder: ParameterEncoder {

    public static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws {
        guard let url = urlRequest.url else { throw NetworkError.missingURL }
        if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false), !parameters.isEmpty {
            urlComponents.queryItems = [URLQueryItem]()
            for (key, value) in parameters {
                let queryItem = URLQueryItem(name: key, value: "\(value)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed))
                urlComponents.queryItems?.append(queryItem)
            }
            urlRequest.url = urlComponents.url
        }
        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
            urlRequest.setValue("application/x-www-from-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        }
    }
}


/*
www.sampleAPI.com/products = 2
