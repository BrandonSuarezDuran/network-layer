//
//  ParameterEncoder.swift
//  Network Layer
//
//  Created by Brandon Suarez on 5/2/21.
//

import UIKit

public typealias Parameters = [String: Any]

public protocol ParameterEncoder {
    static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws
}

public enum NetworkError: String, Error {
    case parameterIsNil = "Parameters were nil"
    case encodingFailed = "Parameters encoding failed"
    case missingURL = "URL is  nil"
}
