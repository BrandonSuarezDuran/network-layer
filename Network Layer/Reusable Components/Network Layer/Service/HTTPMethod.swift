//
//  HTTPMethod.swift
//  Network Layer
//
//  Created by Brandon Suarez on 5/2/21.
//

import UIKit

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
