//
//  HTTPTask.swift
//  Network Layer
//
//  Created by Brandon Suarez on 5/2/21.
//

import UIKit

public typealias HTTPHeaders = [String: String]
public enum HTTPTask {
    case request
    case requestParameters(bodyParameters: Parameters?, urlParameters: Parameters?)
    case requestParametersAndHeaders(bodyParameters: Parameters?, urlParameters: Parameters?, additionHeaders: HTTPHeaders?)
    // Case download, upload...etc
}
