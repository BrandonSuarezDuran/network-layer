//
//  EndPointType.swift
//  Network Layer
//
//  Created by Brandon Suarez on 5/2/21.
//

import UIKit

protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}
